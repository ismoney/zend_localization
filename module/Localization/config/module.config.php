<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Localization;

use Application\Controller\Factory\IndexControllerFactory;
use Application\Middleware\TranslationMiddleware;
use Interop\Container\ContainerInterface;
use Zend\Mvc\I18n\Router\TranslatorAwareTreeRouteStack;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'translator' => array(
		'locale' => 'ru_RU',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),

    'service_manager' => [
        'factories' => [
            TranslationMiddleware::class => function (ContainerInterface $container) {
                return new TranslationMiddleware($container->get('MvcTranslator'));
            },
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'lang' => 'Localization\View\Helper\Lang',
        ],
    ],

];
