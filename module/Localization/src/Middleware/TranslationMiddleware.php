<?php
namespace Localization\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Zend\I18n\Translator\TranslatorInterface;
use Zend\Session\Container;
use Zend\Stratigility\MiddlewareInterface;

class TranslationMiddleware implements MiddlewareInterface
{
    protected $translator;

    protected $langLocale = [
        'ru' => array(
            'lang' => 'russian',
            'locale' => 'ru_RU',
        ),
        'en' => array(
            'lang' => 'english',
            'locale' => 'en_US',
        ),
        'es' => array(
            'lang' => 'espanol',
            'locale'=> 'es_ES',
        ),
        'it' => array(
            'lang' => 'italian',
            'locale'=> 'it_IT',
        ),
        'de' => array(
            'lang' => 'german',
            'locale'=> 'de_DE',
        ),
        'fr' => array(
            'lang' => 'french',
            'locale'=> 'fr_FR',
        )
    ];

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getLocaleByLang($lang)
    {
        foreach ($this->langLocale as $shortLang => $data) {
            if ($lang == $shortLang) {
                return $data['locale'];
            }
        }
    }

    public function __invoke(Request $request, Response $response, callable $out = null)
    {
        $languageSession = new Container('translation');
        $queryParams = $request->getAttribute('lang');
        $path = $request->getUri()->getPath();


        $sessionLanguage = $languageSession->language;
        $defaultLanguage = 'ru';
        $queryLanguage = $queryParams ?? null;
        $currentLanguage = $queryLanguage ?? $sessionLanguage ?? $defaultLanguage;

        if (!$locale = $this->getLocaleByLang($currentLanguage)) {
            return;
        }

        $this->translator->setLocale($locale);
        $languageSession->language = $currentLanguage;

        if(!$queryParams) return $response->withStatus(301)->withHeader('Location', "/$currentLanguage".(strlen($path)>1 ? $path:""));
    }
}