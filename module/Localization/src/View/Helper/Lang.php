<?php
namespace Localization\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container as ContainerSession;

class Lang extends AbstractHelper
{
    function __invoke()
    {
        $SessionContainer = new ContainerSession('translation');
        return $SessionContainer->language;
    }
}