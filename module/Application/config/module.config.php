<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Controller\Factory\IndexControllerFactory;
use Localization\Middleware\TranslationMiddleware;
use Interop\Container\ContainerInterface;
use Zend\Mvc\I18n\Router\TranslatorAwareTreeRouteStack;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'translator' => array(
		'locale' => 'ru_RU',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'router' => [
        'router_class' => TranslatorAwareTreeRouteStack::class,
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'middleware' => TranslationMiddleware::class,
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
            ],

            'localization' => [
                'type' => Segment::class,
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/:lang',
                    'constraints' => array(
                        'lang' => 'ru|en',
                    ),
                    'defaults' => [
                        'middleware' => TranslationMiddleware::class,
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                        'lang' => 'ru',
                    ],
                ),
                'may_terminate' => true,
                'child_routes' => [
                    'application' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/app[/:action]',
                            'defaults' => [
                                //'middleware' => TranslationMiddleware::class,
                                //'__NAMESPACE__' => 'Application\Controller',
                                'action'     => 'test',
                            ],
                        ],
                    ],
                ]
            ]
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => IndexControllerFactory::class,//IndexControllerFactory::class,
        ],

    ],
    'service_manager' => [
        'factories' => [
            TranslationMiddleware::class => function (ContainerInterface $container) {
                return new TranslationMiddleware($container->get('MvcTranslator'));
            },
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'lang' => 'Application\View\Helper\Lang',
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'application/index/test'  =>__DIR__ . '/../view/application/index/test.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
