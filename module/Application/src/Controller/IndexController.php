<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Middleware\TranslationMiddleware;
use Zend\Debug\Debug;
use Zend\I18n\Translator\TranslatorInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\PluginManager;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class IndexController extends AbstractActionController
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function indexAction()
    {

       // print_r($this->lang());
        return new ViewModel();
    }

    public function testAction()
    {
        //$languageSession = new Container('translation');
        //Debug::dump($languageSession->language);

        //Debug::dump($this->translator->getLocale());
        return ;
    }
}
