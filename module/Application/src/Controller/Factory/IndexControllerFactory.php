<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\IndexController;

// Класс фабрики
class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        // Извлечь экземпляр сервиса CurrencyConverter из менеджера сервисов.
        $translator = $container->get('MvcTranslator');

        // Создать экземпляр контроллера и передать сервис в его конструктор.
        return new IndexController($translator);
    }
}