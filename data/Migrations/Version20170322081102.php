<?php

namespace Migrations;

use Admin\Entity\ItemStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Zend\View\Helper\Placeholder\Container;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170322081102 extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // Создаем таблицу 'item'
        $table = $schema->createTable('item');
        $table->addColumn('id', 'integer', ['autoincrement'=>true, 'unsigned'=>true]);
        $table->addColumn('sn', 'string', ['notnull'=>true,'length'=>255]);
        $table->addColumn('order_id', 'integer', ['notnull'=>true]);
        $table->addColumn('item_spec_id', 'integer', ['notnull'=>true]);
        $table->addColumn('start_date', 'datetime', ['notnull'=>false]);
        $table->addColumn('finish_date', 'datetime', ['notnull'=>false]);
        $table->addColumn('is_off', 'integer', ['notnull'=>true,'length'=>1,'default'=>0]);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(array("sn"));
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'computer'
        $table = $schema->createTable('computer');
        $table->addColumn('id', 'integer', ['autoincrement'=>true, 'unsigned'=>true]);
        $table->addColumn('name', 'string', ['notnull'=>true,'length'=>255]);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(array("name"));
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'computer_item'
        $table = $schema->createTable('computer_item');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('computer_id', 'integer', ['notnull'=>true,'unsigned'=>true]);
        $table->addColumn('item_id', 'integer', ['notnull'=>true]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'computer_user'
        $table = $schema->createTable('computer_user');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('computer_id', 'integer', ['notnull'=>true,'unsigned'=>true]);
        $table->addColumn('user_id', 'integer', ['notnull'=>true]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'item_price'
        $table = $schema->createTable('item_price');
        $table->addColumn('id', 'integer', ['autoincrement'=>true, 'unsigned'=>true]);
        $table->addColumn('price', 'integer', ['notnull'=>true]);
        $table->addColumn('price_symbol', 'string', ['notnull'=>true, 'length'=>3]);
        $table->addColumn('item_id', 'integer', ['notnull'=>true]);
        $table->addColumn('active', 'integer', ['length'=>2, 'default'=>1]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'item_spec'
        $table = $schema->createTable('item_spec');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('type_item_id', 'integer', ['notnull'=>true]);
        $table->addColumn('name', 'string', ['notnull'=>true]);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(array("name"));
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'order'
        $table = $schema->createTable('order');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('name', 'string', ['notnull'=>true, 'length'=>45]);
        $table->addColumn('date', 'datetime', ['notnull'=>true]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'price_symbol'
        $table = $schema->createTable('price_symbol');
        $table->addColumn('symbol', 'string', ['autoincrement'=>false, 'length'=>3]);
        $table->setPrimaryKey(['symbol']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'type_item'
        $table = $schema->createTable('type_item');
        $table->addColumn('id', 'integer', ['autoincrement'=>true, 'unsigned'=>true]);
        $table->addColumn('type', 'string', ['notnull'=>true, 'length'=>45]);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(array("type"));
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'user'
        $table = $schema->createTable('user');
        $table->addColumn('id', 'integer');
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'user_item'
        $table = $schema->createTable('user_item');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('item_id', 'integer', ['notnull'=>true]);
        $table->addColumn('user_id', 'integer', ['notnull'=>true]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'item_description'
        $table = $schema->createTable('item_description');
        $table->addColumn('id', 'integer', ['autoincrement'=>true, 'autoincrement'=>true]);
        $table->addColumn('description', 'string', ['notnull'=>false,'length'=>255]);
        $table->addColumn('item_id', 'integer', ['notnull'=>true]);
        $table->addColumn('item_status_id', 'integer', ['notnull'=>true]);
        $table->addColumn('date', 'datetime', ['notnull'=>false]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'item_status'
        $table = $schema->createTable('item_status');
        $table->addColumn('id', 'integer', ['autoincrement'=>true, 'autoincrement'=>true]);
        $table->addColumn('status', 'string', ['notnull'=>true,'length'=>45]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'spec_foto'
        $table = $schema->createTable('spec_foto');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('item_spec_id', 'integer', ['notnull'=>true]);
        $table->addColumn('url', 'string', ['notnull'=>false,'length'=>255]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');

        // Создаем таблицу 'computer_foto'
        $table = $schema->createTable('computer_foto');
        $table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $table->addColumn('computer_id', 'integer', ['notnull'=>true]);
        $table->addColumn('url', 'string', ['notnull'=>false,'length'=>255]);
        $table->setPrimaryKey(['id']);
        $table->addOption('engine' , 'InnoDB');


    }

    public function postUp(Schema $schema)
    {
        $itemStatus =[
            'is_work', // в работе
            'is_free', // свободный
            'is_fix',  // требует ремонта
            'is_dead', // поломан
            //'is_off',  // списан
        ];
        foreach ($itemStatus as $value){
            $sql = "INSERT INTO item_status (status) VALUES ('".$value."')";
            $this->connection->executeQuery($sql);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('items');
        $schema->dropTable('computer');
        $schema->dropTable('computer_item');
        $schema->dropTable('computer_user');
        $schema->dropTable('item_price');
        $schema->dropTable('item_spec');
        $schema->dropTable('order');
        $schema->dropTable('price_symbol');
        $schema->dropTable('type_item');
        $schema->dropTable('user');
        $schema->dropTable('user_item');
        $schema->dropTable('item_description');
        $schema->dropTable('item_status');

    }
}
