<?php
use Zend\Session\Storage\SessionArrayStorage;
use Zend\Session\Validator\RemoteAddr;
use Zend\Session\Validator\HttpUserAgent;

return [
    'doctrine' => [
        // настройка миграций
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => 'data/Migrations',
                'name'      => 'Doctrine Database Migrations',
                'namespace' => 'Migrations',
                'table'     => 'migrations',
            ],
        ],
    ],
    'steam_item_manager' => [
        'languages' => [
            'ru' => 'russian',
            'en' => 'english',
            'de' => 'german'
        ],
    ]
//    'service_manager' => [
//        'factories' => [
//            \Zend\Session\Config\ConfigInterface::class => \Zend\Session\Service\SessionConfigFactory::class,
//
//        ],
//    ],



    // Настройка сессии.
//    'session_config' => [
//        // Срок действия cookie сессии истечет через 1 час.
//        'cookie_lifetime' => 60*60*1,
//        // Данные сессии будут храниться на сервере до 30 дней.
//        'gc_maxlifetime'     => 60*60*24*30,
//    ],
    // Настройка менеджера сессий.
//    'session_manager' => [
//        // Валидаторы сессии (используются для безопасности).
//        'validators' => [
//            RemoteAddr::class,
//            HttpUserAgent::class,
//        ]
//    ],
    // Настройка хранилища сессий.
    /* @see \Zend\Session\Storage\StorageInterface */
//    'session_storage' => [
//        'type' => \Zend\Session\Storage\SessionArrayStorage::class
//    ],

];
